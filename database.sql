\c postgres
drop database SMD;
create database SMD;
\c smd
create type cause_sortie as enum('avarie','naufrage','tempete');

create table sortie_en_mer(
    id SERIAL primary key,
    d date,
    cause cause_sortie
);
create table individu(
    id SERIAL primary key,
    nom varchar(20),
    prenom varchar(20),
    date_naissance date,
    lieu_naissance varchar(50)
);
create type motorisation_bateau as enum('voile','vapeur','rame');
create table bateau(
    id SERIAL primary key,
    name varchar(50),
    construction date,
    motorisation motorisation_bateau
);

create table sauveteur(
    id INT primary key references individu (id),
    debut date,
    fin date default null
);

create table bateau_sauvetage(
    id SERIAL primary key references bateau(id),
    aquisition date
);

create type role_indiv_sauvetage as enum('patron','sous-patron','armement', 'naufragé');
create table sauvetage_humain(
    id SERIAL primary key,
    id_sortie SERIAL references sortie_en_mer(id),
    id_individu SERIAL references individu(id),
    role role_indiv_sauvetage
);

create type role_bateau_sauvetage as enum('sauveur', 'naufragé');
create table sauvetage_embarquation (
    id SERIAL primary key,
    id_sortie SERIAL references sortie_en_mer(id),
    id_embarcation SERIAL references bateau(id),
    role role_bateau_sauvetage
);

create table filiation(
    parent SERIAL references individu(id),
    enfant SERIAL references individu(id),
    primary key (parent,enfant)
);

