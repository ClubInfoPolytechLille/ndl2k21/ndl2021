<html>

	<head>
		<title>Equipe en service</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link href="style.css" rel="stylesheet">
	</head>
	
	<body>
	    <table>
            <thead>
                <td>nom</td>
                <td>prenom</td>
                <td>date de naissance</td>
                <td>lieu de naissance</td>
            </thead>
            <tbody>
                <?php
                    include "includes/connection.php";

                    $result = pg_query($connection, "select * from individu");

                    while ($row = pg_fetch_assoc($result)) {
                        echo "<tr>";
                        echo "<td>".$row['nom']."</td>";
                        echo "<td>".$row['prenom']."</td>";
                        echo "<td>".$row['date_naissance']."</td>";
                        echo "<td>".$row['lieu_naissance']."</td>";
                        echo "</tr>";
                    }
                ?>
            </tbody>
        </table>
	</body>
	
</html>