<html>

	<head>
		<title>Bateaux en services</title>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link href="style.css" rel="stylesheet">
	</head>
	
	<body>
	    <table>
            <thead>
                <td>nom</td>
                <td>date de construction</td>
                <td>motorisation</td>
            </thead>
            <tbody>
                <?php
                    include "includes/connection.php";

                    $result = pg_query($connection, "select * from bateau");
                    while ($row = pg_fetch_assoc($result)) {
                        echo "<tr>";
                        echo "<td>".$row['name']."</td>";
                        echo "<td>".$row['construction']."</td>";
                        echo "<td>".$row['motorisation']."</td>";
                        echo "</tr>";
                    }
                ?>
            </tbody>
        </table>
	</body>
	
</html>