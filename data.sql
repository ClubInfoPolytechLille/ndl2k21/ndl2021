insert into bateau values
    (DEFAULT,'e-cigarette','1819.300','vapeur'),
    (DEFAULT,'hijab','1200.030','voile'),
    (DEFAULT,'alain souchon','1980.050','rame');
insert into bateau_sauvetage values (3,'1200.050');
insert into individu values
    (DEFAULT,'Cook','Tim','1501.001','Douai'),
    (DEFAULT,'Cok','Tim','1502.002','berlin'),
    (DEFAULT,'Cock','Tim','1503.213','Londres'),
    (DEFAULT,'2gamma','Vasco','1403.123','lisbonne'),
    (DEFAULT,'Horn','cap','1819.250','capetown'),
    (DEFAULT,'de Couleur','colorado','1230.053','indiana'),
    (DEFAULT,'Karma','mouche','800.065','Cambrai'),
    (DEFAULT,'Cok O''rico','Tim','1835.077','st-jaques'),
    (DEFAULT,'Nelson','amiral','1984.127','Londres');
insert into sauveteur values 
    (4,'1403.125',NULL),
    (5,'1850.012',null),
    (6,'875.015',null);
insert into sortie_en_mer values (default,'2021.235','avarie');
insert into sauvetage_humain values 
    (default,1,1,'naufragé'),
    (default,1,7,'naufragé'),
    (default,1,4,'armement'),
    (default,1,5,'sous-patron'),
    (default,1,6,'patron');
insert into sauvetage_embarquation values
    (default,1,3,'sauveur'),
    (default,1,2,'naufragé');
insert into filiation values (1,2),(2,3),(3,7),(8,1);